﻿namespace STRIKE7_API_Examples
{
    using System.Drawing;
    using Strike7API;
    using System;
    using System.Diagnostics;
    using System.ComponentModel;
    using System.Linq;
    using System.Management;
    using System.Net.NetworkInformation;
    using System.IO;

    public class MapApplet : Strike7Applet
    {
        private Bitmap m_map = null;
        private int m_x = 0;
        private int m_y = 0;
        private int m_mapWidth;
        private int m_mapHeight;


        private object m_screenUpdateLock = new object();

        private System.Timers.Timer m_screenUpdateTimer = new System.Timers.Timer(25);
        private System.Timers.Timer m_updateTimeTimer = new System.Timers.Timer(1000);

        public MapApplet(Bitmap map)
        {
            m_map = map;
            m_mapWidth = m_map.Width;
            m_mapHeight = m_map.Height;

            m_screenUpdateTimer.Elapsed += Event_ScreenUpdateTimer_Elapsed;
            m_screenUpdateTimer.AutoReset = true;

            m_updateTimeTimer.Elapsed += Event_UpdateTimeTimer_Elapsed;
            m_updateTimeTimer.AutoReset = true;
        }

        public override bool Open(string name, Bitmap image)
        {
            return base.Open(name, image);
        }

        public override void Close()
        {
            m_screenUpdateTimer.Stop();
            base.Close();
        }

        public override void OnAppletActive(int endPointId, long endPointType)
        {
            base.OnAppletActive(endPointId, endPointType);
            UpdateWholeMap();
            m_updateTimeTimer.Start();
        }

        public override void OnAppletInactive(int endPointId)
        {
            base.OnAppletInactive(endPointId);
            m_screenUpdateTimer.Stop();
            m_updateTimeTimer.Stop();
        }

        public override void OnAppletReset(int endPointId)
        {
            base.OnAppletReset(endPointId);
            m_screenUpdateTimer.Stop();
        }



        private void UpdateWholeMap()
        {

            AddTextData();
            UpdateScreen();
        }

        private void UpdateMapSection(int x, int y, int fromX, int fromY, int width, int height)
        {
            Draw(x, y, fromX, fromY, width, height, m_map);
        }

        private void ClearTextData()
        {
            lock (m_screenUpdateLock)
            {
                UpdateMapSection(0, 0, m_x, m_y, 500, 20);
                UpdateMapSection(0, 20, m_x, m_y + 20, 500, 20);
                UpdateMapSection(0, 40, m_x, m_y + 40, 500, 20);
                UpdateMapSection(0, 60, m_x, m_y + 60, 500, 20);
                UpdateMapSection(0, 80, m_x, m_y + 80, 500, 20);
                UpdateMapSection(0, 100, m_x, m_y + 100, 500, 20);
                UpdateMapSection(0, 120, m_x, m_y + 120, 500, 20);
                UpdateMapSection(0, 140, m_x, m_y + 140, 500, 20);
                UpdateMapSection(0, 160, m_x, m_y + 160, 500, 20);
                UpdateMapSection(0, 180, m_x, m_y + 180, 500, 20);
                UpdateMapSection(0, 200, m_x, m_y + 200, 500, 20);
                UpdateMapSection(0, 220, m_x, m_y + 220, 500, 20);
                UpdateMapSection(0, 240, m_x, m_y + 240, 500, 20);
                UpdateMapSection(0, 260, m_x, m_y + 260, 500, 20);
                UpdateMapSection(0, 280, m_x, m_y + 280, 500, 20);
                UpdateMapSection(0, 300, m_x, m_y + 300, 500, 20);
                
                
            }
        }

        private System.OperatingSystem m_osInfo = System.Environment.OSVersion;

        private void AddTextData()
        {
            lock (m_screenUpdateLock)
            {
                int count = 0;
                Process[] prs = Process.GetProcesses();
                foreach (Process proces in prs)
                {
                    count++;
                }
                count = count - 1;
                int[] cores = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                double[] ram = new double[5] { 0, 0, 0, 0, 0 };
                double[] drive = new double[5] { 0, 0, 0, 0, 0 };
                var core1 = new PerformanceCounter("Processor", "% Processor Time", "0");
                var core2 = new PerformanceCounter("Processor", "% Processor Time", "1");
                var core3 = new PerformanceCounter("Processor", "% Processor Time", "2");
                var core4 = new PerformanceCounter("Processor", "% Processor Time", "3");
                var core5 = new PerformanceCounter("Processor", "% Processor Time", "4");
                var core6 = new PerformanceCounter("Processor", "% Processor Time", "5");
                var core7 = new PerformanceCounter("Processor", "% Processor Time", "6");
                var core8 = new PerformanceCounter("Processor", "% Processor Time", "7");
                var load_percentage = new PerformanceCounter("Processor", "% Processor Time", "_Total");
                var ramused = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
                //var ramtotal = new System.Diagnostics.PerformanceCounter("Memory", "_Total");

                core1.NextValue();
                core2.NextValue();
                core3.NextValue();
                core4.NextValue();
                core5.NextValue();
                core6.NextValue();
                core7.NextValue();
                core8.NextValue();
                load_percentage.NextValue();

                ramused.NextValue();
   
                System.Threading.Thread.Sleep(1000);
                
                cores[1] = (int)core1.NextValue();
                cores[2] = (int)core2.NextValue();
                cores[3] = (int)core3.NextValue();
                cores[4] = (int)core4.NextValue();
                cores[5] = (int)core5.NextValue();
                cores[6] = (int)core6.NextValue();
                cores[7] = (int)core7.NextValue();
                cores[8] = (int)core8.NextValue();
                cores[9] = (int)load_percentage.NextValue();

                //RAM

                ManagementClass mc = new ManagementClass("Win32_ComputerSystem");
                ManagementObjectCollection moc = mc.GetInstances();
                foreach (ManagementObject item in moc)
                {
                    ram[2] = Math.Round(Convert.ToDouble(item.Properties["TotalPhysicalMemory"].Value) / 1073741824, 2);
                }



                ram[0] = (double)ramused.NextValue(); //free ram
                ram[0] = ram[0] / 1024;               //free ram
                ram[0] = Math.Round(ram[0], 2);       //free ram

                ram[1] = ram[2] - ram[0]; //Used ram

                ram[3] = Math.Round(((100 * ram[1]) / ram[2]), 2);
                             
                //NETWORK

                
                NetworkInterface[] nicArr;
                nicArr = NetworkInterface.GetAllNetworkInterfaces();
                NetworkInterface nic = nicArr[0];
                IPv4InterfaceStatistics interfaceStats = nic.GetIPv4Statistics();
             

                int bytesSentSpeed = (int)(interfaceStats.BytesSent) / 1048576;
                int bytesReceivedSpeed = (int)(interfaceStats.BytesReceived) / 1048576;
            
                //HARD DISK

                drive[0] = GetTotalHDDSize("C:\\") - GetTotalFreeSpace("C:\\");








                DrawText(new Point(0, 0), 18, Color.White, "******************************CPU INFO*******************************");
                DrawText(new Point(0, 30), 18, Color.White, " CORE 1: " + cores[1] + "% " + "CORE 2: " + cores[2] + "% " + "CORE 3: " + cores[3] + "% " + "CORE 4: " + cores[4] + "%" );
                DrawText(new Point(0, 50), 18, Color.White, " CORE 5: " + cores[5] + "% " + "CORE 6: " + cores[6] + "% " + "CORE 7: " + cores[7] + "% " + "CORE 8: " + cores[8] + "%");
                DrawText(new Point(0, 70), 18, Color.White, " NUMBER OF PROCESS: " + count + " LOAD: " + cores[9] + "%" );
                DrawText(new Point(0, 100), 18, Color.White, "***************************RAM INFO*******************************");
                DrawText(new Point(0, 130), 18, Color.White, " USAGE: " +ram[3] +"% " + "TOTAL: " + ram[2] + "GB");
                DrawText(new Point(0, 150), 18, Color.White, " USED: " + ram[1] + "GB " + "FREE: " + ram[0] +" GB");
                DrawText(new Point(0, 180), 18, Color.White, "***************************NET USAGE******************************");
                DrawText(new Point(0, 210), 18, Color.White, "TOTAL SENT: " + bytesSentSpeed + " MB " + "TOTAL RECEIVED: " + bytesReceivedSpeed +" MB");
                DrawText(new Point(0, 240), 18, Color.White, "**************************DISK USAGE*****************************");
                DrawText(new Point(0, 270), 18, Color.White, " CAPACITY: " + GetTotalHDDSize("C:\\") + "GB" + " USED: " + drive[0]+ "GB" + " FREE: " + GetTotalFreeSpace("C:\\") + "GB");
                DrawText(new Point(0, 300), 18, Color.White, "*********************************************************************");
                
                

            }
        }

        public double GetTotalHDDSize(string driveName)
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == driveName)
                {
                    return drive.TotalSize / (1024 * 1024 * 1024);
                }
            }
            return -1;
        }

        public double GetTotalFreeSpace(string driveName)
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == driveName)
                {
                    return drive.TotalFreeSpace / (1024 * 1024 * 1024);
                }
            }
            return -1;
        }

        private static String getRAMsize()
        {
            ManagementClass mc = new ManagementClass("Win32_ComputerSystem");
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject item in moc)
            {
                return Convert.ToString(Math.Round(Convert.ToDouble(item.Properties["TotalPhysicalMemory"].Value) / 1073741824, 2)) + " GB";
            }

            return "RAMsize";
        }

        public int GetCpuUsage()
        {
            int[] cores = new int[8] {0,0,0,0,0,0,0,0};
            var core1 = new PerformanceCounter("Processor", "% Processor Time", "1");
            var core2 = new PerformanceCounter("Processor", "% Processor Time", "2");
            core1.NextValue();
            core2.NextValue();
           // System.Threading.Thread.Sleep(1000);
            cores[1] = (int)core1.NextValue();
            return cores[8];
        }

        

        private void Event_ScreenUpdateTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            AddTextData();
            //UpdateScreenForScroll();
        }

        private void Event_UpdateTimeTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            lock (m_screenUpdateLock)
            {
                ClearTextData();
                AddTextData();
                UpdateScreen();
            }
        }
    }
}
